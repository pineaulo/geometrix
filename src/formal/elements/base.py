#!/bin/python
# coding: utf-8

class Expr:
	def __init__(self, sym=None):
		self.sym=sym
		if sym!=None:
			self.to_latex=lambda : self.sym  

class Scalar(Expr):
	def __init__(self, value, sym=None):
		Expr.__init__(self,sym=sym)
		self.value=value

	def to_latex(self):
		if self.value.__class__ == int:
			return str(self.value)
		if self.value.__class__ ==  complex:
			value= str(self.value).replace("j","i")
			if value[0]=='(':
				value= value[1:-1]
			if self.value.real == 1:
				value= value[:-2]+"i"
			return value

def normalizedIndex(*index):
	if index.__len__() == 1:
		return index[0]-1 if index[0]>0 else index[0]
	return [i-1 if i>0 else i for i in index]