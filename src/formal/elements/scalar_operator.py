#!/bin/python
# coding: utf-8

from .base import Expr

class Sum(Expr):
	def __init__(self, left, right):
		self.left=left
		self.right=right

class Product(Expr):
	def __init__(self, left, right):
		self.left=left
		self.right=right

class Frac(Expr):
	def __init__(self, numerator, denominator):
		self.numerator=numerator
		self.denominator=denominator

	def to_latex(self,latex_engine):
		return r"\frac{"+latex_engine(self.numerator)+"}{"+latex_engine(self.denominator)+"}"
