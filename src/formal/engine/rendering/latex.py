# coding: utf-8
from formal.elements import base

def latex(expr):
	try:
		return expr.to_latex()
	except TypeError as e:
		return expr.to_latex(latex)
	except AttributeError as e:
		if expr.__class__ in [int, complex]:
			return base.Scalar(expr).to_latex()
		raise e