#!/bin/python
# coding: utf-8
import sys, getopt
import test.expression_building 
import test.latex_rendering 

def usage():
	print("Usage:\n\t-h : Show help\n\t-q : Quiet mode\n\t-t [test_id] : Run tests")
	print("test_id:\n\texpr  : buid expression\n\tlatex : render expression in latex")

run_test={
	"latex": test.latex_rendering.run_test,
	"expr": test.expression_building.run_test
	}

if __name__=="__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],"qaht:")
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	verbose_mode=True
	if ("-q", '') in opts:
		verbose_mode=False
	if ("-h", '') in opts:
		if verbose_mode: usage()
		exit(0)
	if ("-a", '') in opts:
		test_list = run_test.keys()
	else:
		test_list = []
		for opt, arg in opts:
			if "-t"==opt:
				test_list.append(arg)
	#Time to run all the tests !
	for test_id in test_list:
		if verbose_mode: print("Running test ["+test_id+"]")
		try:
			if run_test[test_id]():
				if verbose_mode: print("Test succeded !")
			else:
				if verbose_mode: print("Test failed...")
				exit(1)
		except KeyError:
			print("Unknow test \"%s\""% test_id)
			exit(1)


