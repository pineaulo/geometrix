BUILD_DIR = build
OUTPUT_TEST_DIR = test/build
VALIDATE_TEST_DIR = test/validate

.PHONY: all init validate test coverage

init: 
	pip install -r requirements.txt

validate: 
	mv $(OUTPUT_TEST_DIR)/*.out.txt $(VALIDATE_TEST_DIR)/

test: 
	python3 test.py -a
	diff $(OUTPUT_TEST_DIR)/ $(VALIDATE_TEST_DIR)/
	@echo "All test are passed."

coverage:
	coverage run test.py -aq
	coverage report

clean:
	\rm -f *~
	\find $(OUTPUT_TEST_DIR)/ -mindepth 1 -maxdepth 1 -type f -name 'test_*.out.txt'  -delete
	\rm -f .coverage