# coding: utf-8
import formal.engine.rendering.latex as l
import formal.elements.base as base
import formal.elements.scalar_operator as operator

from . import _build_dir_

_test_file_ = _build_dir_+"/test_latex.out.txt"

def run_test():
	test_list=[
		base.Expr(sym="x"),
		142,
		2j,
		2j-3,
		base.Scalar(2j-3),
		base.Scalar(2j-3, sym="x"),
		operator.Frac(1,6j),
		operator.Frac(1+1j,6j-2)
		]
	error_list=[
		"String",
		operator.Product(1,15),
		operator.Sum(1,15)
		]
	passed = True
	with open(_test_file_,"w") as f:
		for expr in test_list:
			try:
				f.write(l.latex(expr)+"\n")
			except Exception as e:
				f.write("Error")
				print(e.__class__.__name__+": "+str(e))
				passed=False
		for expr in error_list:
			try:
				f.write(l.latex(expr)+"\n")
				passed=False
			except Exception as e:
				f.write(e.__class__.__name__+": "+str(e)+"\n")
	return passed