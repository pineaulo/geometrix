# coding: utf-8
import formal.elements.base as base

from . import _build_dir_

_test_file_ = _build_dir_+"/test_expr.out.txt"

def run_test():
	passed = True
	with open(_test_file_,"w") as f:
		f.write("None")
	return passed